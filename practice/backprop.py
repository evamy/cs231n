# Backpropagation Multiply Gate


class MultiplyGate(object):

    def forward(self, x, y):
        self.x = x
        self.y = y
        z = x * y
        return z

    def backward(self, dz):
        dx = self.y * dz
        dy = self.x * dz
        return dx, dy

x, y = 5, 4
step = 0.01

z = MultiplyGate()
zo = z.forward(x, y)
dz = 1
dx, dy = z.backward(dz)

x = x + step * dx
y = y + step * dy

print z.forward(x, y)
