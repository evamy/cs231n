# Neural Network

import math
import numpy as np


class NeuralNetwork(object):

    def function(W, x, b):
        self.W = W
        self.x = x
        self.b = b

        y = W * x + b
        return y

    def activation(x):
        sigmoid = 1 / (1 + math.exp((-1) * x))
        return sigmoid

    def update_W(W, x, y):
        W = W + x * y
        return W

    def update_b(b, y):
        b = b + y
        return b


# x = ... #from training data
# y = ... #from training data
W = np.zeros((len(x), len(y)))
b = np.zeros((len(y)))

nn = NeuralNetwork()
W = nn.update_W(W, x, y)
b = nn.update_b(b, y)

# new_x

print function(W, new_x, b)
