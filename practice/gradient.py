# Gradient
import numpy as np


def numerical_gradient(f, x):
	fx = f(x)
	h = 0.0001
	grad = np.zeros()