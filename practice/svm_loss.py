# SVM Loss

import numpy as np


def svm_loss(x, y, W):
    loss = np.array(x[0])
    for i in xrange(len(x)):
        loss[i] = L_i_vectorised(x, y, W)


def L_i_vectorised(x, y, W):
    scores = np.dot(W, x)
    margins = max(0, scores - scores[y] + 1)
    margins[y] = 0
    loss_i = np.sum(margins)
    return loss_i

def full_loss(x, y, W, lamda = 0.31623):

    L = svm_loss(x, y, W)
    loss = sum(L) / len(L) + lamda * np.square(W)

    return loss
