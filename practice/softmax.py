# SOFTMAX
import numpy as np


def softmax(scores):
    exp = np.array(scores)
    exp = np.exp(scores)

    return np.divide(exp, sum(exp))


def full_loss(x, y, W, lamda = 0.31623):

    scores = np.dot(W, x)
    L = softmax(scores)

    L = np.array(L)
    L = -np.log10(L)
    loss = sum(L) / len(L)

    return loss
